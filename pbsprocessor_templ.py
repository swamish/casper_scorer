import sys

# read pbs_template and split it at variables that need to be added. 
# Variables defined by the term VAR
# For changes that need to be made to the pbs script, make changes to the template

# Defining some variables and assigning parameters taken from command line arguments
pbs_template = sys.argv[1]      # the original template
cliqscount = sys.argv[2]        # taken from a count of the .cliq.gpdb files produced after ParseBack
workingdir = sys.argv[3]        # the working dir where this particular pdb is being scored. 
# Also is the outfol where all the .clique files will be placed later
sizeofcliqs = sys.argv[4]              # size of each clique being scored in this pdb
rmsd_cutoff = sys.argv[5]       # rmsd_cutoff for superimposition of any clique
penalty = sys.argv[6]           # penalty upon bad or no superimposition
randompicking_N = sys.argv[7]   # max number of cliques being superimposed against each test clique
percentageseqID = sys.argv[8]
maxCASPERcutoff = sys.argv[9]

if workingdir.endswith("/")!=True:
    pdbname=workingdir
    workingdir = workingdir + "/"
else:
    pdbname=workingdir[:-1]

def getFileNameWithoutExtension(path):
    # this function does exactly what it's name says (duh!)
    return path.split('\\').pop().split('/').pop().rsplit('.', 1)[0]

# Note: total number of cliqs is not equal to total number of groups in the file
# clique with all groups from same residue will not be considered 
# so total number of cliques is actually a little less than 
# the total number of chemical groups in the protein

with open(pbs_template, 'r') as pbstemplf:
    pbstemplf = (pbstemplf.read()).split("VAR")

pdbname_sans_ext = getFileNameWithoutExtension(pdbname)

# read rest of the variables from the command line arguments 
# and add them in between the split list called pbstemplf
pbstemplf = pbstemplf[0] + percentageseqID + \
            pbstemplf[1] + maxCASPERcutoff + \
            pbstemplf[2] + pdbname_sans_ext.lower()[:4] + \
            pbstemplf[3] + randompicking_N + \
            pbstemplf[4] + sizeofcliqs + \
            pbstemplf[5] + rmsd_cutoff + \
            pbstemplf[6] + penalty +\
            pbstemplf[7] + workingdir + "cliquescores/" +\
            pbstemplf[8]

print(workingdir + pdbname_sans_ext)
with open(workingdir + 'pbsprocessing.sh', "w") as pbscriptf:
    pbscriptf.write(pbstemplf)

