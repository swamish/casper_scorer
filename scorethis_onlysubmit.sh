#!/bin/bash

# take a pdb as input and scores all its cliques on the cluster then returns the data as output files(?)
pdbfiles=$1

# take values of n and dmax for finding cliques and creating pdb files for each
size=$2
dmax=$3

# take values of rmsd_cutoff for superimposition
rmsd=$4
#penalty for bad or no superimposition
penalty=$5
#no of cliques to superimpose against per clique in each pdb
sampling=$6
maxCASPERcutoff=$7

newpbsscorerslist=$8
#################################################################################

#echo $(date +"%F %T"): pdbfiles taken from $pdbfiles. Cliques are of size at max $size . \
    #Max distance from centre of clique to edge is $dmax, and RMSD cutoff is $rmsd . \
    #Penalty for no match with with this criteria is $penalty . \
    #$sampling cliques being matched at max, per clique being scored, per pdb

#bin_dir="/home/swastik/bin/"
#gpdbparser=$bin_dir"/casper_scorer/gpdbparser.py"
#starfinder=$bin_dir"/casper_scorer/starfinder.py"
#cliqgpdbparser=$bin_dir"/casper_scorer/parseback.py"
#pbstemplatesh=$bin_dir"/casper_scorer/scoreradcliqs_templ_cl.sh"
#pbswriterpy=$bin_dir"/casper_scorer/scoreradcliqs_createpbs_cl.py"
#cliqtocliqwisepy=$bin_dir"/casper_scorer/get_cliqwisescores.py"
#subprocesswriterpy=$bin_dir"/casper_scorer/pbsprocessor_templ.py"
#subprocesstemplate=$bin_dir"/casper_scorer/pbsprocessor_templ.sh"
#rejectpdbsfinder=$bin_dir"/casper_scorer/rejectpdbs_finder.sh"

#perc=30             # max percentage similarity cutoff. Anything with similarity higher than this gets added 
                    ## to rejectpdbfile and is not superimposed against in the subjob

## here we go...
## for each pdb given as input
#for pdbfile in $(cat $pdbfiles);do

    #basepdbfilename=$(basename $pdbfile)            # basename of pdbfilename 
    #thisworkdir=$(basename $pdbfile .pdb)"_"$size"_"$dmax"_"$rmsd"_"$penalty"_"$sampling
    #superbasepdbfilename=$(basename $pdbfile .pdb)    # basename of the pdbfilename, without the extension
    #filepathname=$(dirname $pdbfile)                  # dir where pdbfile resides

    ## make working directory for this specific pdbfile in the currentdir and move there
    #if [ -d $thisworkdir  ];then
        #echo Warning: $thisworkdir directory already exists. Exiting.
        #exit 1
    #else
        #mkdir $thisworkdir
        #cd $thisworkdir
    #fi
    ## make folder for all input and output
    #mkdir -p erroutfiles
    ## make dir for clique files 
    #mkdir -p cliquescores
    ## make gpdb file out of the pdbfile
    #python3 $gpdbparser -i $basepdbfilename -s $filepathname -d $(realpath ./)

    ## run python script RadCliques2.py to find all star-cliques in the pdbfile (python script stored in /storage/swastik/scripts/)
    ## and store the .cliq file in the workingdir
    #python3 $starfinder -i $superbasepdbfilename".gpdb" -n $size -d $dmax -c $(realpath ./)"/"

    ## Parse all the star-cliques to .cliq.gpdb format (ParseBack.py script stored in /storage/swastik/scripts)
    ## store them all in workingdir
    #python3 $cliqgpdbparser -i $superbasepdbfilename".cliq" -g $(realpath ./) -c $(realpath ./)

    #numcliqgpdbs=$(ls *.cliq.gpdb |wc -l) &&
    ##########
    #echo $(date +"%F %T"): no of cliq.gpdb files being scored for $superbasepdbfilename is $numcliqgpdbs
    ##########
    ## for each .cliq.gpdb file, score the file by creating a pbs script for this pdbfile 
    ## and scoring all the .cliq.gpdb files in parallel
    ## return the scores to workingdir

    #python3 $pbswriterpy $pbstemplatesh $numcliqgpdbs $(realpath ./)"/" &&
        #python3 $subprocesswriterpy $subprocesstemplate $numcliqgpdbs $(realpath ./)"/" $size $rmsd $penalty $sampling $perc $maxCASPERcutoff

    ## pdbID of the pdbfile in caps
    #superpdbupp=$(echo $superbasepdbfilename |tr '[:lower:]' '[:upper:]')
    #superpdbupper=${superpdbupp::4}
    #echo /storage/swastik/db/rejectpdblists/rejectpdblists_$perc/$superpdbupper"_"$perc"_rejectpdbs.txt" 
    #if [ -e /storage/swastik/db/rejectpdblists/rejectpdblists_$perc/$superpdbupper"_"$perc"_rejectpdbs.txt"  ];then
        #cp /storage/swastik/db/rejectpdblists/rejectpdblists_$perc/$superpdbupper"_"$perc"_rejectpdbs.txt" ./
    #else
        #echo "rejectpdb file not found. Downloading..."
        #bash $rejectpdbsfinder $perc $superpdbupper
    #fi && 

    ## after pdbfile's cliques have been scored, find scores group-wise and residue-wise(?)
    #cd ..
## end for loop that loops over each pdb file given as input
#done &&
#########
#echo $(date +"%F %T"): All working directories created for job submissions. Submitting jobs sequentially...
#########

#actually score the pdbs by submitting jobs one after the other
echo new pbsscorer.sh list $newpbsscorerslist
for pbsscript in $(cat ./$newpbsscorerslist|sort -r) ;do
    exitcode=1
    # while job has finished, submit new job
    out=$(qsub $pbsscript) &&
    echo $(date +"%F %T"): $out Job submitted for $pbsscript
    while [ ! $exitcode -eq 0 ];do                  # while exitcode for the qstat is 1, job hasn't finished, sleep 5s and try again 
        b=$(qstat -f $out 2>&1 |grep "Job has finished\|Unknown Job Id")    # run qstat to check if job has finished
        exitcode=$?
        sleep 5
    done &&
    echo $(date +"%F %T"): $pbsscript "Job Complete" in $out
done &&

# cat all clique scores together for each pdb and tar all .out and .err files of the jobs
# then delete the folder containing the files used for the job and the outputs of the job
for pdbfile in $(cat $pdbfiles);do
    dirname=$(basename $pdbfile .pdb)"_"$size"_"$dmax"_"$rmsd"_"$penalty"_"$sampling

    cd $dirname &&
    tail -n +1 cliquescores/*|grep ".clique\|SCORE\|Top RMSD" > ../$dirname.cliquescores &&
    cd ..
    echo "Scores written for $dirname."
done &&

echo $(date +"%F %T"): Scores written. Tar necessary files and remove the rest...
tar -zcf cliquefiles_$size"_"$dmax"_"$rmsd"_"$penalty"_"$sampling".tar.gz" */cliquescores/ &&
tar -zcf jobs_errout_$size"_"$dmax"_"$rmsd"_"$penalty"_"$sampling".tar.gz" */erroutfiles/ &&

#rm -r */ & disown
echo $(date +"%F %T"): Fin.

