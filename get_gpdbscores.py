import sys

'''
Uses .cliqscores file to output .gpdbscores file
Usage: get_gpdbscores.py cliqscores-file gpdb-file cliq-file
'''

cliqscores_file=sys.argv[1]
gpdb_file=sys.argv[2]
cliq_file=sys.argv[3]
penalty=sys.argv[4]

# reads the cliqscores file
with open(cliqscores_file, 'r') as cscoresf:
    readscores=cscoresf.read()
    # split based on -------- so that the name of the clique scored
    # and the superimposition data are separate but consecutive
    readscores=readscores.split("--------")
    for i,j in enumerate(readscores):
        readscores[i]=j.strip()
    if readscores[0]=="" or readscores[0]==None:
        del readscores[0]

with open(gpdb_file,'r') as gpdbf:
    gpdbf = gpdbf.readlines()
    gpdbdict = {x:[] for x in range(1, len(gpdbf)+1)}


cliqdict={}
# reads cliq file to find out how many cliques were scored and give default value=penalty to all of the cliques initially
with open(cliq_file, 'r') as cliqf:
    cliqf=cliqf.readlines()
    cliqscoresdict={x:float(penalty) for x in range(len(cliqf))}
    
    # create dictionary for cliqs
    for i,j in enumerate(cliqf):
        cliqkey=j.split()[1]
        cliqval=j.split()[1:]
        cliqdict[int(cliqkey)]=cliqval

# for each clique scored and data stored in readscores list, assign it's data to appropriate key in the cliqscoresdict dictionary
for i in range(len(readscores)):
    if i%2==0:
        thekey=int(readscores[i].split("_")[0])-1
        try:
            thevalue=float(readscores[i+1].split("RMSD is:")[-1])
        except ValueError as e:
            if (readscores[i+1].split("RMSD is:")[-1]).strip()=="":         # this means that the cliqs file was empty and somehow was still incorporated
                # so the .clique file has a string of spaces instead of a number
                # penalise such a situation directly, since this means that no star-clique was found that could match
                # print "thevalue is: nothing"
                thevalue = 5.0
            else:
                print(e)
            # print e, "\n"
            # print i, readscores[i]
        cliqscoresdict[thekey]=thevalue
        # print i, thekey, ":", thevalue

# print cliqscoresdict

for i,j in enumerate(cliqdict.keys()):
    for k in cliqdict[j]:
        gpdbdict[int(k)+1].append(j)

# for i in sorted(cliqdict.keys())[:10]:
    # print i, cliqdict[i]

# for i in sorted(gpdbdict.keys())[:10]:
    # print i, gpdbdict[i]

gpdbscores={}
for i,gpdbkey in enumerate(sorted(gpdbdict.keys())):
    fullscore = 0.0
    noofcliqs = 0
    for smcliq in gpdbdict[gpdbkey]:
        # print "cliq and score: ", smcliq, cliqscoresdict[smcliq]
        fullscore += cliqscoresdict[smcliq]
        noofcliqs +=1
        # print "fullscore and no of cliqs:", fullscore, noofcliqs
    gpdbval = fullscore/noofcliqs
    gpdbscores[gpdbkey] = gpdbval
    # print "key and value", gpdbkey, ":", gpdbval

outfilename = ".".join(cliqscores_file.split(".")[:-1])+".gpdbscores"
with open(outfilename, 'w') as outf:
    for i in sorted(gpdbscores.keys()):
        # print i, ":", gpdbscores[i]
        outf.write(str(i)+'\t'+str(gpdbscores[i])+'\n')

