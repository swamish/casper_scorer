#!/usr/bin/env bash

# directory where everything is being computed in
filevar=$1

# If the folder /tmp/workingswastik doesn't exist then make the folder
if [ ! -d "/tmp/workingswastik" ];then
mkdir /tmp/workingswastik
fi

# If $filevar already exists, it's from a previous job so delete it
if [ -d "/tmp/workingswastik/"$filevar ]; then
    echo $filevar "directory exists. Deleting this directory"
    rm -rf /tmp/workingswastik/$filevar
fi &&

# Make a new folder named $filevar
mkdir -p /tmp/workingswastik/$filevar &&
# Current directory
cd /tmp/workingswastik/$filevar &&


# copy these stuff:
    # 3dLS-SM for superimposition
    # usgroupID underscore delimited composition of this query clique
scriptsdir="/home/swastik/bin/casper_scorer/"
ls3d="gdtscorer/bin/starscorer.bin"
#usgroupID="FindGroupIDUS.py"

# copy required programs to subjob directory
cp $scriptsdir/$ls3d /tmp/workingswastik/$filevar &&

#rejectpdbstoggle=1
perc=VAR
maxCASPERcutoff=VAR
pdbID=VAR

# parameters
N=VAR         # no of cliqs this sub-job will score each test clique against              # need to optimise
size=VAR      # size of cliques, both model/test clique and the clique being tested against
rmsd=VAR      # rmsd cutoff of superimposition
penalty=VAR   # penalty upon bad or no superimposition

gpdbdir="/storage/swastik/db/gpdb_db_2018dec15"
starsdir="/storage/swastik/db/stars_db_2018dec15/nr$perc/stars_"$size"_centered/"
rejectpdbdir="/storage/swastik/db/rejectpdblists/rejectpdblists_"$perc/
#nrdir="/storage/swastik/db/nr_clusters/bc_pc/"

# sneaky boi reading this. -_- Look away unless you are supposed to read this script

# this is the output folder in /storage/swastik where the output files from all jobs\
   # are being transferred to at the end of the sub-job
outfol=VAR &&

#compo=$(python3 /tmp/workingswastik/$filevar/FindGroupIDUS.py $filevar".cliq.gpdb" $size) &&        # full compo
rejectpdbsfile=$rejectpdbdir/$(echo $pdbID|awk '{print toupper($0)}')"_"$perc"_rejectpdbs.txt"
rejectPDBslist=$(printf "$(cat $rejectpdbsfile)"|paste -d"," -s)

# dirname of $outfol is the workingdir where this pdb is being scored and processed files are being transferred to
querydir=$(dirname $outfol) &&
    cliqno=$(echo $filevar | awk -F "_" '{print $1}') &&
    gpdbfilenamebase=$(echo $filevar | awk -F "_" -v "SIZE=$size" '{for(i=2; i<=NF-SIZE; i++){print $i}}'|paste -s -d "_")

echo querydir is $querydir and cliqno is $cliqno and gpdbfilenamebase is $gpdbfilenamebase

# if there exists a 'reject_pdbs.txt' for this job then grep for the $compo.cliqs leaving out the pdbs in the reject_pdbs.txt;
# else shuffle directly in $compo.cliqs and pick N lines out of it

queryclique=$(echo $(sed -n "${cliqno}{p;q;}" $querydir/$gpdbfilenamebase".cliq")) &&
    echo queryclique is $queryclique &&
    #matchedclique=$(printf "$queryclique\n" | ./starscorer.bin -q $querydir -t $gpdbdir -c $starsdir -n $size -r $rmsd -s $N -p $penalty -j $pdbID -g $maxCASPERcutoff |tee -a $outfol/$filevar.clique 2>&1) &&
    matchedclique=$(printf "$queryclique\n" | ./starscorer.bin -q $querydir -t $gpdbdir -c $starsdir -n $size -r $rmsd -s $N -p $penalty -j $pdbID $rejectPDBslist -g $maxCASPERcutoff |tee -a $outfol/$filevar.clique 2>&1) &&
echo $(date +"%F %T"): all superimpositions finished

echo $(date +"%F %T"): Sub-job processes ended

# put an rm process, disown and end subjob; leave
rm -r /tmp/workingswastik/$filevar & disown
